//store/index.js
export const state = () => ({
    data: true
})

export const mutations = {
    setData(state, data) {
        state.data = data
    }
}

export const getters = {
    getData(state) {
        return state.data
    }
}