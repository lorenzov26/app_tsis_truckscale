export default function({ route, redirect }) {
  let modules;

  if (localStorage && localStorage.modules) {
    let flag = false;
    modules = JSON.parse(localStorage.modules);

    for (let i = 0; i < modules.length; i++) {
      for (let i2 = 0; i2 < modules[i].actions.length; i2++) {
        if (
          modules[i].modulestatus === true &&
          modules[i].actions[i2].actionname === route.meta[0].access.right &&
          modules[i].actions[i2].actionstatus === true
        ) {
          flag = true;
        }
      }
    }

    if (!flag) {
      localStorage.clear()
      redirect("/");
    }
  } else {
    redirect("/");
  }
}
